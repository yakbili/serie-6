package exo13;

import java.util.ArrayList;
//import java.util.numsayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		List<String> nums = new ArrayList<String>();

		nums.add("one");
		nums.add("two");
		nums.add("three");
		nums.add("four");
		nums.add("five");
		nums.add("six");
		nums.add("seven");
		nums.add("eight");
		nums.add("nine");
		nums.add("ten");
		nums.add("eleven");
		nums.add("twelve");
		
		Stream<String> strings = nums.stream();
		
		/****QUESTION 1****/
		Integer longestNum = nums.stream()
				.mapToInt(s -> s.length())
				.max()
				.getAsInt();
		
		System.out.println("/****QUESTION 1****/");
		System.out.println(longestNum);
		
		/****QUESTION 2****/
		Long pairNum = nums.stream()
				//.mapToLong(num -> (num.length())%2)
				.filter(num -> (((num.length())%2)==0))
				.count();
				
		System.out.println("/****QUESTION 2****/");
		System.out.println(pairNum);

	
		
		/****QUESTION 3****/
		List<String> concatList = nums.stream()
				
				.filter(num -> !(((num.length())%2)==0))
	            .collect(Collectors.toList());
	    
		concatList.replaceAll(String::toUpperCase);
		System.out.println("/****QUESTION 3****/");
		concatList.forEach(System.out::println); 
		
		/****QUESTION 4****/
		String prefix = "{";
		String concatString = prefix.concat(nums.stream()
				
				.filter(num -> num.length()<=5)
				.sorted()
	            .reduce((num1, num2) -> num1 + ", " + num2)
	            .get()
	            .concat("}"));
		
		System.out.println("/****QUESTION 4****/");
		System.out.println(concatString);
		
		/****QUESTION 5****/
		Map<Integer, List<String>> hm1  = nums.stream()
				.collect(Collectors.groupingBy(s->s.length()));
		
		System.out.println("/****QUESTION 5****/");
		System.out.println(hm1);

		

	}

}
