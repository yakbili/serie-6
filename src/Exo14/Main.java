package Exo14;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		Germinal ger = new Germinal();
		
		/****QUESTION 1****/
		int nbrLines = ger.readLinesGer().size();
		System.out.println("/****QUESTION 1****/");
		System.out.println(nbrLines);
		
		/****QUESTION 2****/
		int nbrLinesNoEmpty = ger.readLinesGer().stream()
				.filter(line -> !line.isEmpty())
				.collect(Collectors.toList())
				.size();
		
		System.out.println("/****QUESTION 2****/");
		System.out.println(nbrLinesNoEmpty);
		
		/****QUESTION 3****/
		BiFunction<String,String, Integer> nbrBonjourLine = (string, mot) ->string
				.replaceAll(" ","")
				.toLowerCase()
				.concat("?")
				.split(mot)
				.length-1;
		
		Integer nbreBonjour = ger.readLinesGer().stream()
				.mapToInt(s -> nbrBonjourLine.apply(s, "bonjour"))
				.sum();
		
		System.out.println("/****QUESTION 3****/");
		System.out.println(nbreBonjour);
		
		/****QUESTION 4****/
		Function<String, Stream<Character>> strToCharFunc = string->Arrays.stream(string.split(""))
				.filter(s->!s.isEmpty())
				.map(s->s.charAt(0));
				//s ->s.chars().mapToObj(); --------> fonctionne mais on force... 
			
		/****QUESTION 5****/
		Stream<Character> streamCharGer = strToCharFunc.apply(ger.readLinesGer().toString());
		
		/****QUESTION 6****/
		
		List<Character> listCharGer = streamCharGer.distinct()
				.sorted()
				.collect(Collectors.toList());
		
		System.out.println("/****QUESTION 6****/");
		System.out.println(listCharGer);
		
		BiFunction<String, String, Stream<String>> splitWordWithPattern =
				(line, pattern) ->
					Pattern.compile("[" + pattern + "]").splitAsStream(line);
		
		Stream<String> streamDeTousLesMots =  splitWordWithPattern
				.apply(ger.readLinesGer().toString()," '!.?,_*;<>\\-");
				
		
		/****QUESTION 7****/
		long nombreMotsGerminal = splitWordWithPattern.apply(ger.readLinesGer().toString(), " '!.?,_*;<>\\-")
				.count();
		
		System.out.println("/****QUESTION 7****/");
		System.out.println(nombreMotsGerminal);
		
		
		long nombreMotsGerminalDifferent = splitWordWithPattern.apply(ger.readLinesGer().toString(), " '!.?,_*;<>\\-")
				.distinct()
				.count();
		
		System.out.println(nombreMotsGerminalDifferent);
		
		/****QUESTION 8****/
		Integer longueurMotPlusLong = splitWordWithPattern
				.apply(ger.readLinesGer().toString(), " '!.?,_*;<>\\-")
				.distinct()
				.mapToInt(s -> s.length())
				.max()
				.getAsInt();
		
		System.out.println("/****QUESTION 8****/");
		System.out.println(longueurMotPlusLong);
		
		long nbreMotsLongueurMotPlusLong = splitWordWithPattern
				.apply(ger.readLinesGer().toString(), " '!.?,_*;<>\\-")
				.distinct()
				.filter(s -> s.length() == longueurMotPlusLong)
				.count();
		
		System.out.println(nbreMotsLongueurMotPlusLong);
		
		
		List<String> ListMotsDeLongueurMotPlusLong = splitWordWithPattern
				.apply(ger.readLinesGer().toString(), " '!.?,_*;<>\\-")
				.distinct()
				.filter(s -> s.length() == longueurMotPlusLong)
				.collect(Collectors.toList());
		
		System.out.println(ListMotsDeLongueurMotPlusLong);
						
		
		
		
		
		
		
				
			
		

	}
}
	
	


