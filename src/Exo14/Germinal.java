package Exo14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Germinal {
	public  List<String> readLinesFrom(String fileName) {
		Path path = Paths.get(fileName);
		try(Stream<String> lines = Files.lines(path)){
			return lines.collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public  List<String> readLinesGer(){
		List<String> lines = readLinesFrom("germinal");
		return lines.stream()
				.limit(lines.size()-322)
				.skip(70)
				.collect(Collectors.toList());
}
}
